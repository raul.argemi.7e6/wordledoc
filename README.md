# Wordle

El proyecto Wordle es un juego en el que tienes que adivinar una palabra oculta de cinco letras, teniendo seis intentos para ello. El objetivo del juego es acertar la palabra, y para ello tienes que ir introduciendo diferentes palabras y el programa te indicará cuántas letras de las que has introducido se corresponden con la palabra oculta.

# Requisitos

Este proyecto está desarrollado con el lenguaje de programación Kotlin y utiliza algunas funciones de la biblioteca estándar de Java.

# Cómo jugar

Para jugar al Wordle, hay que abrir la aplicación y seguir los siguientes pasos:

1. Seleccionar el idioma. El juego está disponible en catalán y castellano. Para seleccionar el idioma, tendrás que introducir "1" para catalán o "2" para castellano.

2. El programa te dará la bienvenida y te explicará las reglas del juego.

3. Tendrás que introducir una palabra de cinco letras. El programa te indicará cuántas letras se corresponden con la palabra oculta.

4. Repite el paso 3 hasta que hayas acertado la palabra o hayas agotado los seis intentos disponibles.

5. Una vez que hayas terminado, el programa te mostrará las estadísticas de la partida, incluyendo el número de partidas jugadas, el número de palabras adivinadas, el número de palabras fallidas y la mayor racha de palabras adivinadas en una sola partida.

# Variables

El juego utiliza las siguientes variables universales:

intentosAdivinarPalabra: contador del número de intentos necesarios para adivinar la palabra.
rachaPalabrasAdivinadas: contador de la racha de palabras adivinadas en una misma partida.
palabrasResueltas: contador del número de palabras resueltas.
palabrasFallidas: contador del número de palabras fallidas.
partidas: contador del número de partidas jugadas.
rachaTotal: contador de la mayor racha de palabras adivinadas en todas las partidas.
palabraIntroducida: variable para almacenar la palabra que introduce el usuario.
words: lista de palabras disponibles.
idioma: idioma seleccionado por el usuario.
intentos: contador del número de intentos disponibles en una misma partida.
verde: cadena de caracteres que se utiliza para dar formato al color verde en la consola.
amarillo: cadena de caracteres que se utiliza para dar formato al color amarillo en la consola.
gris: cadena de caracteres que se utiliza para dar formato al color gris en la consola.
black: cadena de caracteres que se utiliza para dar formato al color negro en la consola.
reset: cadena de caracteres que se utiliza para reiniciar los colores en la consola.
finalizar: bandera que se utiliza para indicar si el usuario desea finalizar la partida.
partidasGanadas: contador del número de partidas ganadas.
partidasPerdidas: contador del número de partidas perdidas.

# Conclusion

Cambiar o modificar un codigo sin rehacerlo todo de nuevo es muy complicado, y sobretodo en este caso teniendo en cuenta que el codigo lo empeze cuando mis conocimientos eran mucho menos que los de hoy dia. Lo cual ha complicado bastante el poder meter esta nueva "expansion" del wordle.
