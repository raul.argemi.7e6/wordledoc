import java.io.File
import java.time.LocalDate
import kotlin.io.path.Path
import kotlin.io.path.createDirectory
import kotlin.io.path.notExists

//Creacion de variables universales
var intentosAdivinarPalabra = 0
var rachaPalabrasAdivinadas = 0
var palabrasResueltas = 0
var palabrasFallidas = 0
var partidas = 0
var rachaTotal = 0
var palabraIntroducida:String = ""
var words = mutableListOf<String>()
var idioma = "castella"
var intentos = 0
var verde = "\u001b[48;2;0;204;0m"
var amarillo = "\u001b[48;2;240;234;39m"
var gris = "\u001b[48;2;160;160;160m"
var black = "\u001b[38;2;0;0;0m"
var reset = "\u001b[m"
var finalizar = false
//Para las estadisticas

var partidasGanadas = 0
var partidasPerdidas = 0

fun cargarDiccionario(idioma: String) {

    words = arrayListOf()

    val fitxer = File("wordle2/diccionaris/$idioma.txt")

    fitxer.forEachLine { words.add(it) }
}

/**
 * Funcion para elegir el idioma
 */
fun cambiarIdioma() {

    println("--------------------------------------")
    println("Elige el idioma")
    println("1. Catalan")
    println("2. Castellano")
    when (readln()) {
        "1" -> {
            cargarDiccionario("catala")
            idioma = "català"
        }
        "2" -> {
            cargarDiccionario("castella")
            idioma = "castellà"
        }
        else -> println("Elige un idioma que este incluido")
    }
}


/**
 * Inicializa las variables del juego.
 *
 * Muestra un mensaje de bienvenida al usuario.
 */
fun inicio() {
    println("~~~~~~~~~~~~~~~~~~~~~~")
    println("BIENVENIDO A WORDLE")
    println("~~~~~~~~~~~~~~~~~~~~~~")
    println("Tienes que adivivinar la palabra de 5 letras oculta, tienes 6 intentos para adivinar la palabra")
    println("\n")
}

/**
 * Muestra las estadísticas del juego.
 *
 * La función muestra en pantalla el número de partidas jugadas, el número de palabras adivinadas,
 * el número de palabras fallidas y la mayor racha de palabras adivinadas en una sola partida.
 *
 * @param partidas el número de partidas jugadas.
 * @param palabrasResueltas el número de palabras adivinadas.
 * @param palabrasFallidas el número de palabras fallidas.
 * @param rachaTotal la mayor racha de palabras adivinadas en una sola partida.
 */
fun Stats() {
    if (palabrasResueltas >= 1) {
        println("Llevas una racha de: $rachaPalabrasAdivinadas")
        println("Llevas resueltas: " + palabrasResueltas + " palabras")
        println("Llevas $palabrasFallidas palabras fallidas")
        println("Tu media de intentos para adivinar una palabra es de: " + intentosAdivinarPalabra.toFloat() / partidas)
        println("Tu porcentaje de victorias es: " + palabrasResueltas.toFloat() / partidas * 100 + "%")
        println("Tu racha total es: $rachaTotal")
    } else {
        println("Llevas una racha de: $rachaPalabrasAdivinadas")
        println("Llevas resueltas: " + palabrasResueltas + " palabras")
        println("Llevas $palabrasFallidas palabras fallidas")
        println("Tu media de intentos para adivinar una palabra es nula de momento.")
        println("Tu porcentaje de victorias es del: 0%")
        println("Tu racha total es: $rachaTotal")
    }
}

/**
 * Pregunta al usuario si desea volver a jugar y devuelve su respuesta.
 *
 * La función muestra un mensaje en pantalla preguntando al usuario si desea volver a jugar y
 * espera a que el usuario ingrese su respuesta. Si el usuario ingresa "s" o "S", la función
 * devuelve true, en cualquier otro caso devuelve false.
 *
 * @return true si el usuario desea volver a jugar, false en caso contrario.
 */
fun jugardenuevo(): Boolean {
    println("¿Quieres jugar otra vez? (s/n)")
    val respuesta = readln().toString()
    return respuesta == "s"
}
/**
 * Ejecuta una ronda del juego Wordle.
 *
 * En cada ronda, se selecciona una palabra aleatoria a adivinar y se le pide al usuario que adivine
 * la palabra a través de la consola. La función muestra en pantalla las letras que coinciden o no
 * con la palabra a adivinar y lleva el registro del progreso del juego.
 *
 * @param palabraAAdivinar la palabra que el usuario debe adivinar en esta ronda.
 * @param words la lista completa de palabras disponibles.
 * @param verde, amarillo, gris, black, reset cadenas de caracteres que se utilizan para dar formato a la salida en pantalla.
 * @param rachaPalabrasAdivinadas, palabrasResueltas, palabrasFallidas, intentosAdivinarPalabra, rachaTotal variables que se utilizan para llevar el registro del progreso del juego y mostrar estadísticas al final de cada ronda.
 *
 * @return true si el usuario adivinó la palabra en esta ronda, false en caso contrario.
 */
fun ronda(
    palabraAAdivinar: String,
    words: MutableList<String>,
    verde: String,
    amarillo: String,
    gris: String,
    black: String,
    reset: String,
) {
    var intentos = 0
    var palabraIntroducida: String
    var palabraAAdivinar = words.random()
    do {
        palabraIntroducida = readln().toString().toLowerCase()
        if ((palabraIntroducida.length == 5) && (palabraIntroducida in words)) {
            var j = 0
            var letrasUsadas = mutableListOf<Char>()
            for (i in palabraIntroducida) {
                if (palabraAAdivinar[j] == i) {
                    print(verde + black + i + reset)
                    letrasUsadas.add(i)
                } else if (palabraAAdivinar[j] in palabraIntroducida && palabraAAdivinar[j] != i && !letrasUsadas.contains(i)) {
                    print(amarillo + black + i + reset)
                    letrasUsadas.add(i)
                } else {
                    print(gris + black + i + reset)
                }
                j++
            }
            intentos++
            intentosAdivinarPalabra++
            println("\n" + "Llevas $intentos intentos" + "\n")
        } else {
            if ((palabraIntroducida.length != 5) || (palabraIntroducida !in words)) {
                println("Palabra incorrecta, introduce una palabra de 5 letras, o una palabra correcta. No se te restarán intentos.")
            }
        }
    } while (intentos <= 5 && palabraIntroducida != palabraAAdivinar)
    if (palabraIntroducida != palabraAAdivinar) {
        println("Te has quedado sin intentos, la palabra era $palabraAAdivinar")
        words.remove(palabraAAdivinar)
        rachaPalabrasAdivinadas = 0
        partidasPerdidas++
        palabrasFallidas++
        intentosAdivinarPalabra = intentosAdivinarPalabra - 6
    } else{
        println("Has acertado!")
        partidasGanadas++
        words.remove(palabraAAdivinar)
        rachaPalabrasAdivinadas++
        palabrasResueltas++
        if(rachaTotal<rachaPalabrasAdivinadas)rachaTotal=rachaPalabrasAdivinadas

    }
}


/**
 * Funcion que gaurda el historico
 * @param nombre El nombre del usuario de las estadisticas
 */
fun guardarEstadisticas(nombre: String){

    val dir = Path("wordle2/historico/")
    if (dir.notExists()){ dir.createDirectory() }

    val fitxer = File("wordle2/historico/$nombre.txt")
    if (!fitxer.exists()){fitxer.createNewFile()}

    val data = LocalDate.now()

    fitxer.appendText("$data;$partidasGanadas;$partidasPerdidas;$rachaTotal;$palabrasResueltas\n")


}


/**
 * Muestra las estadisticas historicas
 * @param nombre Nombre del usuario
 */
fun mostrarEstadistiques(nombre: String) {


    val fitxer = File("wordle2/historico/$nombre.txt")
    println("################ $nombre ################  ")


    fitxer.forEachLine {


        val dades = it.split(";")
        val data = dades[0]
        val partidasGanadas = dades[1].toInt()
        val partidasPerdidas = dades[2].toInt()
        val rachaTotal = dades[3].toInt()
        val palabrasResueltas = dades[4].toInt()


        println("###########################################")
        println("Data: $data")
        println("Partidas ganadas: $partidasGanadas")
        println("Partidas perdidas: $partidasPerdidas")
        println(
            "Porcentaje de victorias: ${if (partidasGanadas == 0) "Tu porcentaje de victorias es del 0" else palabrasResueltas.toFloat() / partidas * 100}%"
        )
        println("Media de intentos: ${if (partidasGanadas == 0) "Tu media de intentos para adivinar una palabra es nula de momento." else intentosAdivinarPalabra.toFloat() / partidas}")
        println("Mejor racha total: $rachaTotal")


    }
}

/**
 * Menu
 */
fun menu(): String {
    var palabraAAdivinar = words.random()
    mostrarMenu()
    val respuesta = readln()
    when (respuesta) {

        "1" -> cambiarIdioma()
        "2" -> do {
            partidas++
            println("Introduce una palabra:")
            ronda(palabraAAdivinar.toString(), words, verde, amarillo, gris, black, reset)
            Stats()
        } while (jugardenuevo())

        "3" -> pedirNombreUsuario()
        "salir" -> {println("Has salido de la aplicacion.")
        }
        else -> println("Elige un valor valido")
    }
    return respuesta
}

/**
 * Pide un usuario para mostrar su historico
 */
fun pedirNombreUsuario() {
    println("Escribe el usuario del que quieres ver las estadisticas")
    val nombre = readln()

    val fitxer = File("wordle2/historico/$nombre.txt")
    if (fitxer.exists()){

        mostrarEstadistiques(nombre)
    } else {

        println("Escribe un nombre registrado")
    }

}


fun mostrarMenu() {
    println()
    println("################################################")
    println("1. Elige el idioma. (Idioma por defecto español)")
    println("2. Empieza el juego")
    println("3. Ver historicos")
    println("salir. Salir del programa")
}


/**
 * Punto de entrada del juego Wordle.
 *
 * La función `main` es el punto de entrada del juego Wordle. Inicializa las variables del juego,
 * muestra un mensaje de bienvenida al usuario y comienza una ronda de juego. Después de cada ronda,
 * pregunta al usuario si desea volver a jugar y, si es así, vuelve a iniciar una ronda. Si el usuario
 * decide no volver a jugar, la función muestra las estadísticas finales del juego y termina.
 */
fun main() {
    println("###########")
    println("Bienvenido a Wordle")
    println("###########")
    println("Introduce tu nombre:")
    var nombre = readln()
    inicio()
    cargarDiccionario("castella")
    do {
        var respuesta = menu()
    }while (respuesta != "salir")
    guardarEstadisticas(nombre)
}


